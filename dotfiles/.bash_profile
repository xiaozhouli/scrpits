#------------------------------------------------------------
#
# This file holds all my BASH configurations and aliases
# Author: Xiaozhou Li
# Creat Time:   2014-07-22
# Last Changed: 2016-03-10

# Section:
#   ->  Environment Configuration
#   ->  Better Termial (remapping defaults and adding functionality)
#   ->  Added by other installer
#
#------------------------------------------------------------

#----------------------------------------
#   Environment Configuration
#----------------------------------------

# Change Prompt
export PS1="________________________________________________________________________________\n| \w @ \h (\u) \n| \! > "
export PS2="| > "

# Color Setting
export CLICOLOR=1
export LSCOLOR=gxBxhxDxfxhxhxhxhxcxcx

# Set default blocksize for ls, df, du
export BLOCKSIZE=1k

#----------------------------------------
#   Better Termial
#----------------------------------------

alias cp='cp -iv'
alias mv='mv -iv'
alias mkdir='mkdir -pv'
alias ll='ls -FGhp'
alias cd..='cd ../'
alias ..='cd ../'
alias ...='cd ../../'
alias .3='cd ../../../'
alias f='open -a Finder ./'

# extract: Extract most know archives with one command
# ----------------------------------------------------
    extract () {
        if [ -f $1 ]; then
            case $1 in
                *.tar.bz2)  tar xjf $1      ;;
                *.tar.gz)   tar xzf $1      ;;
                *.bz2)      bunzip2 $1      ;;
                *.rar)      unrar e $1      ;;
                *.gz)       gunzip $1       ;;
                *.tar)      tar xf $1       ;;
                *.tbz2)     tar xjf $1      ;;
                *.tgz)      tar xzf $1      ;;
                *.zip)      unzip $1        ;;
                *.Z)        uncompress $1   ;;
                *.7z)       7z x $1         ;;
                *)      echo "'$1' cannot be extracted via extract()" ;;
            esac
        else
            echo "'$1' is not a vaild file"
        fi
    }

#----------------------------------------
#   Added by other installer
#----------------------------------------
# added by Anaconda3 2.0.1 installer
export PATH="/Users/xiaozhouli/anaconda/bin:$PATH"

export PATH="/Users/xiaozhouli/Library/Application Support/GoodSync":$PATH

export PATH="/Library/Application Support/GoodSync":$PATH

#export PATH="/Users/xiaozhouli/Documents/Xiaozhou/PostDoc/Project/moose/gui":$PATH


##
# Your previous /Users/xiaozhouli/.bash_profile file was backed up as /Users/xiaozhouli/.bash_profile.macports-saved_2015-11-05_at_11:10:35
##

# MacPorts Installer addition on 2015-11-05_at_11:10:35: adding an appropriate PATH variable for use with MacPorts.
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
# Finished adapting your PATH environment variable for use with MacPorts.

export PATH="$HOME/bin:$PATH"


##
# Your previous /Users/xiaozhouli/.bash_profile file was backed up as /Users/xiaozhouli/.bash_profile.macports-saved_2015-12-06_at_13:43:55
##

# MacPorts Installer addition on 2015-12-06_at_13:43:55: adding an appropriate PATH variable for use with MacPorts.
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
# Finished adapting your PATH environment variable for use with MacPorts.


# Uncomment to enable pretty prompt:
# export MOOSE_PROMPT=true

# Uncomment to enable autojump:
# export MOOSE_JUMP=true

# Source MOOSE profile
#if [ -f /opt/moose/environments/moose_profile ]; then
        #. /opt/moose/environments/moose_profile
#fi

#export PATH=/Users/xiaozhouli/Documents/Xiaozhou/PostDoc/Project/moose/gui:$PATH

# mount operations on CSCS
alias hpcmount='umount /Users/xiaozhouli/Documents/Xiaozhou/PostDoc/Project/CSCS/home ;
sshfs xiaozhou@ela.cscs.ch: /Users/xiaozhouli/Documents/Xiaozhou/PostDoc/Project/CSCS/home ; '
alias hpcunmount='umount /Users/xiaozhouli/Documents/Xiaozhou/PostDoc/Project/CSCS/home ; '

# PETSc
# PETSc 3.6.3
export PETSC_DIR=/Users/xiaozhouli/Documents/Xiaozhou/Research/Soft/petsc-3.6.3
export PETSC_ARCH=arch-darwin-c-debug
# PETSc (Mac ports)
#export PETSC_DIR=/opt/local/lib/petsc
