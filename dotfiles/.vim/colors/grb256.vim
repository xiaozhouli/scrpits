




<!DOCTYPE html>
<html class="   ">
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# object: http://ogp.me/ns/object# article: http://ogp.me/ns/article# profile: http://ogp.me/ns/profile#">
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    
    <title>dotfiles/.vim/colors/grb256.vim at master · garybernhardt/dotfiles</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub" />
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub" />
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-114.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-144.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144.png" />
    <meta property="fb:app_id" content="1401488693436528"/>

      <meta content="@github" name="twitter:site" /><meta content="summary" name="twitter:card" /><meta content="garybernhardt/dotfiles" name="twitter:title" /><meta content="dotfiles - ~grb. Things in here are often interdependent. A lot of stuff relies on scripts in bin/." name="twitter:description" /><meta content="https://avatars3.githubusercontent.com/u/45707?s=400" name="twitter:image:src" />
<meta content="GitHub" property="og:site_name" /><meta content="object" property="og:type" /><meta content="https://avatars3.githubusercontent.com/u/45707?s=400" property="og:image" /><meta content="garybernhardt/dotfiles" property="og:title" /><meta content="https://github.com/garybernhardt/dotfiles" property="og:url" /><meta content="dotfiles - ~grb. Things in here are often interdependent. A lot of stuff relies on scripts in bin/." property="og:description" />

    <link rel="assets" href="https://github.global.ssl.fastly.net/">
    <link rel="conduit-xhr" href="https://ghconduit.com:25035/">
    <link rel="xhr-socket" href="/_sockets" />

    <meta name="msapplication-TileImage" content="/windows-tile.png" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="selected-link" value="repo_source" data-pjax-transient />
    <meta content="collector.githubapp.com" name="octolytics-host" /><meta content="collector-cdn.github.com" name="octolytics-script-host" /><meta content="github" name="octolytics-app-id" /><meta content="82A1D237:2E4B:17AA26B:535A28E1" name="octolytics-dimension-request_id" /><meta content="7149641" name="octolytics-actor-id" /><meta content="xiaozhouli" name="octolytics-actor-login" /><meta content="b11500184d4528a8b4d79ad11d43a148b92410e90a9796ecedca9ca73ca05a93" name="octolytics-actor-hash" />
    

    
    
    <link rel="icon" type="image/x-icon" href="https://github.global.ssl.fastly.net/favicon.ico" />

    <meta content="authenticity_token" name="csrf-param" />
<meta content="mdo1KKnMHPbq30MjERbqsQZx+Wr4Uq0zP3LLVciGQtCk2+Va0XzMiqK7sBbtT9M09u8f4g1LwqcOIxaGE/TMmA==" name="csrf-token" />

    <link href="https://github.global.ssl.fastly.net/assets/github-0fccb0d1330ec12e83e33d762026ca69cd2b6862.css" media="all" rel="stylesheet" type="text/css" />
    <link href="https://github.global.ssl.fastly.net/assets/github2-50d481402ffe2c088905b9982f2846f5f1be9b43.css" media="all" rel="stylesheet" type="text/css" />
    


        <script crossorigin="anonymous" src="https://github.global.ssl.fastly.net/assets/frameworks-f8f8d8ee1afb4365ba5e002fdbc3c8e61738713b.js" type="text/javascript"></script>
        <script async="async" crossorigin="anonymous" src="https://github.global.ssl.fastly.net/assets/github-0dce5cbbd453992693347ff7a62aa955cf152870.js" type="text/javascript"></script>
        
        
      <meta http-equiv="x-pjax-version" content="f8ae4599f3ae8a183e55831ae86406ce">

        <link data-pjax-transient rel='permalink' href='/garybernhardt/dotfiles/blob/d7bd2abb2e5af19ab5c1800320d2488a0f31cefc/.vim/colors/grb256.vim'>

  <meta name="description" content="dotfiles - ~grb. Things in here are often interdependent. A lot of stuff relies on scripts in bin/." />

  <meta content="45707" name="octolytics-dimension-user_id" /><meta content="garybernhardt" name="octolytics-dimension-user_login" /><meta content="1147007" name="octolytics-dimension-repository_id" /><meta content="garybernhardt/dotfiles" name="octolytics-dimension-repository_nwo" /><meta content="true" name="octolytics-dimension-repository_public" /><meta content="false" name="octolytics-dimension-repository_is_fork" /><meta content="1147007" name="octolytics-dimension-repository_network_root_id" /><meta content="garybernhardt/dotfiles" name="octolytics-dimension-repository_network_root_nwo" />
  <link href="https://github.com/garybernhardt/dotfiles/commits/master.atom" rel="alternate" title="Recent Commits to dotfiles:master" type="application/atom+xml" />

  </head>


  <body class="logged_in  env-production linux vis-public page-blob">
    <a href="#start-of-content" tabindex="1" class="accessibility-aid js-skip-to-content">Skip to content</a>
    <div class="wrapper">
      
      
      
      


      <div class="header header-logged-in true">
  <div class="container clearfix">

    <a class="header-logo-invertocat" href="https://github.com/">
  <span class="mega-octicon octicon-mark-github"></span>
</a>

    
    <a href="/notifications" aria-label="You have no unread notifications" class="notification-indicator tooltipped tooltipped-s" data-gotokey="n">
        <span class="mail-status all-read"></span>
</a>

      <div class="command-bar js-command-bar  in-repository">
          <form accept-charset="UTF-8" action="/search" class="command-bar-form" id="top_search_form" method="get">

<div class="commandbar">
  <span class="message"></span>
  <input type="text" data-hotkey=" s" name="q" id="js-command-bar-field" placeholder="Search or type a command" tabindex="1" autocapitalize="off"
    
    data-username="xiaozhouli"
      data-repo="garybernhardt/dotfiles"
      data-branch="master"
      data-sha="1e3431dc8869063f643663b70cc68b2a1af9e8f6"
  >
  <div class="display hidden"></div>
</div>

    <input type="hidden" name="nwo" value="garybernhardt/dotfiles" />

    <div class="select-menu js-menu-container js-select-menu search-context-select-menu">
      <span class="minibutton select-menu-button js-menu-target" role="button" aria-haspopup="true">
        <span class="js-select-button">This repository</span>
      </span>

      <div class="select-menu-modal-holder js-menu-content js-navigation-container" aria-hidden="true">
        <div class="select-menu-modal">

          <div class="select-menu-item js-navigation-item js-this-repository-navigation-item selected">
            <span class="select-menu-item-icon octicon octicon-check"></span>
            <input type="radio" class="js-search-this-repository" name="search_target" value="repository" checked="checked" />
            <div class="select-menu-item-text js-select-button-text">This repository</div>
          </div> <!-- /.select-menu-item -->

          <div class="select-menu-item js-navigation-item js-all-repositories-navigation-item">
            <span class="select-menu-item-icon octicon octicon-check"></span>
            <input type="radio" name="search_target" value="global" />
            <div class="select-menu-item-text js-select-button-text">All repositories</div>
          </div> <!-- /.select-menu-item -->

        </div>
      </div>
    </div>

  <span class="help tooltipped tooltipped-s" aria-label="Show command bar help">
    <span class="octicon octicon-question"></span>
  </span>


  <input type="hidden" name="ref" value="cmdform">

</form>
        <ul class="top-nav">
          <li class="explore"><a href="/explore">Explore</a></li>
            <li><a href="https://gist.github.com">Gist</a></li>
            <li><a href="/blog">Blog</a></li>
          <li><a href="https://help.github.com">Help</a></li>
        </ul>
      </div>

    


  <ul id="user-links">
    <li>
      <a href="/xiaozhouli" class="name">
        <img alt="xiaozhouli" class=" js-avatar" data-user="7149641" height="20" src="https://avatars1.githubusercontent.com/u/7149641?s=140" width="20" /> xiaozhouli
      </a>
    </li>

    <li class="new-menu dropdown-toggle js-menu-container">
      <a href="#" class="js-menu-target tooltipped tooltipped-s" aria-label="Create new...">
        <span class="octicon octicon-plus"></span>
        <span class="dropdown-arrow"></span>
      </a>

      <div class="new-menu-content js-menu-content">
      </div>
    </li>

    <li>
      <a href="/settings/profile" id="account_settings"
        class="tooltipped tooltipped-s"
        aria-label="Account settings ">
        <span class="octicon octicon-tools"></span>
      </a>
    </li>
    <li>
      <form class="logout-form" action="/logout" method="post">
        <button class="sign-out-button tooltipped tooltipped-s" aria-label="Sign out">
          <span class="octicon octicon-log-out"></span>
        </button>
      </form>
    </li>

  </ul>

<div class="js-new-dropdown-contents hidden">
  

<ul class="dropdown-menu">
  <li>
    <a href="/new"><span class="octicon octicon-repo-create"></span> New repository</a>
  </li>
  <li>
    <a href="/organizations/new"><span class="octicon octicon-organization"></span> New organization</a>
  </li>


</ul>

</div>


    
  </div>
</div>

      

        



      <div id="start-of-content" class="accessibility-aid"></div>
          <div class="site" itemscope itemtype="http://schema.org/WebPage">
    <div id="js-flash-container">
      
    </div>
    <div class="pagehead repohead instapaper_ignore readability-menu">
      <div class="container">
        

<ul class="pagehead-actions">

    <li class="subscription">
      <form accept-charset="UTF-8" action="/notifications/subscribe" class="js-social-container" data-autosubmit="true" data-remote="true" method="post"><div style="margin:0;padding:0;display:inline"><input name="authenticity_token" type="hidden" value="/Jf2aVncTqTSYMBsaqjLvJ6LaFC4iNim3AZCheSPVlFAYvKrmnGrCR3NOp+CBaXhxHJax/ivtY3uejPhkHhgiQ==" /></div>  <input id="repository_id" name="repository_id" type="hidden" value="1147007" />

    <div class="select-menu js-menu-container js-select-menu">
      <a class="social-count js-social-count" href="/garybernhardt/dotfiles/watchers">
        73
      </a>
      <span class="minibutton select-menu-button with-count js-menu-target" role="button" tabindex="0" aria-haspopup="true">
        <span class="js-select-button">
          <span class="octicon octicon-eye-watch"></span>
          Watch
        </span>
      </span>

      <div class="select-menu-modal-holder">
        <div class="select-menu-modal subscription-menu-modal js-menu-content" aria-hidden="true">
          <div class="select-menu-header">
            <span class="select-menu-title">Notification status</span>
            <span class="octicon octicon-remove-close js-menu-close"></span>
          </div> <!-- /.select-menu-header -->

          <div class="select-menu-list js-navigation-container" role="menu">

            <div class="select-menu-item js-navigation-item selected" role="menuitem" tabindex="0">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <div class="select-menu-item-text">
                <input checked="checked" id="do_included" name="do" type="radio" value="included" />
                <h4>Not watching</h4>
                <span class="description">You only receive notifications for conversations in which you participate or are @mentioned.</span>
                <span class="js-select-button-text hidden-select-button-text">
                  <span class="octicon octicon-eye-watch"></span>
                  Watch
                </span>
              </div>
            </div> <!-- /.select-menu-item -->

            <div class="select-menu-item js-navigation-item " role="menuitem" tabindex="0">
              <span class="select-menu-item-icon octicon octicon octicon-check"></span>
              <div class="select-menu-item-text">
                <input id="do_subscribed" name="do" type="radio" value="subscribed" />
                <h4>Watching</h4>
                <span class="description">You receive notifications for all conversations in this repository.</span>
                <span class="js-select-button-text hidden-select-button-text">
                  <span class="octicon octicon-eye-unwatch"></span>
                  Unwatch
                </span>
              </div>
            </div> <!-- /.select-menu-item -->

            <div class="select-menu-item js-navigation-item " role="menuitem" tabindex="0">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <div class="select-menu-item-text">
                <input id="do_ignore" name="do" type="radio" value="ignore" />
                <h4>Ignoring</h4>
                <span class="description">You do not receive any notifications for conversations in this repository.</span>
                <span class="js-select-button-text hidden-select-button-text">
                  <span class="octicon octicon-mute"></span>
                  Stop ignoring
                </span>
              </div>
            </div> <!-- /.select-menu-item -->

          </div> <!-- /.select-menu-list -->

        </div> <!-- /.select-menu-modal -->
      </div> <!-- /.select-menu-modal-holder -->
    </div> <!-- /.select-menu -->

</form>
    </li>

  <li>
  

  <div class="js-toggler-container js-social-container starring-container ">
    <a href="/garybernhardt/dotfiles/unstar"
      class="minibutton with-count js-toggler-target star-button starred"
      aria-label="Unstar this repository" title="Unstar garybernhardt/dotfiles" data-remote="true" data-method="post" rel="nofollow">
      <span class="octicon octicon-star-delete"></span><span class="text">Unstar</span>
    </a>

    <a href="/garybernhardt/dotfiles/star"
      class="minibutton with-count js-toggler-target star-button unstarred"
      aria-label="Star this repository" title="Star garybernhardt/dotfiles" data-remote="true" data-method="post" rel="nofollow">
      <span class="octicon octicon-star"></span><span class="text">Star</span>
    </a>

      <a class="social-count js-social-count" href="/garybernhardt/dotfiles/stargazers">
        771
      </a>
  </div>

  </li>


        <li>
          <a href="/garybernhardt/dotfiles/fork" class="minibutton with-count js-toggler-target fork-button lighter tooltipped-n" title="Fork your own copy of garybernhardt/dotfiles to your account" aria-label="Fork your own copy of garybernhardt/dotfiles to your account" rel="nofollow" data-method="post">
            <span class="octicon octicon-git-branch-create"></span><span class="text">Fork</span>
          </a>
          <a href="/garybernhardt/dotfiles/network" class="social-count">281</a>
        </li>


</ul>

        <h1 itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="entry-title public">
          <span class="repo-label"><span>public</span></span>
          <span class="mega-octicon octicon-repo"></span>
          <span class="author"><a href="/garybernhardt" class="url fn" itemprop="url" rel="author"><span itemprop="title">garybernhardt</span></a></span><!--
       --><span class="path-divider">/</span><!--
       --><strong><a href="/garybernhardt/dotfiles" class="js-current-repository js-repo-home-link">dotfiles</a></strong>

          <span class="page-context-loader">
            <img alt="Octocat-spinner-32" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
          </span>

        </h1>
      </div><!-- /.container -->
    </div><!-- /.repohead -->

    <div class="container">
      <div class="repository-with-sidebar repo-container new-discussion-timeline js-new-discussion-timeline  ">
        <div class="repository-sidebar clearfix">
            

<div class="sunken-menu vertical-right repo-nav js-repo-nav js-repository-container-pjax js-octicon-loaders">
  <div class="sunken-menu-contents">
    <ul class="sunken-menu-group">
      <li class="tooltipped tooltipped-w" aria-label="Code">
        <a href="/garybernhardt/dotfiles" aria-label="Code" class="selected js-selected-navigation-item sunken-menu-item" data-gotokey="c" data-pjax="true" data-selected-links="repo_source repo_downloads repo_commits repo_tags repo_branches /garybernhardt/dotfiles">
          <span class="octicon octicon-code"></span> <span class="full-word">Code</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>


      <li class="tooltipped tooltipped-w" aria-label="Pull Requests">
        <a href="/garybernhardt/dotfiles/pulls" aria-label="Pull Requests" class="js-selected-navigation-item sunken-menu-item js-disable-pjax" data-gotokey="p" data-selected-links="repo_pulls /garybernhardt/dotfiles/pulls">
            <span class="octicon octicon-git-pull-request"></span> <span class="full-word">Pull Requests</span>
            <span class='counter'>0</span>
            <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>


        <li class="tooltipped tooltipped-w" aria-label="Wiki">
          <a href="/garybernhardt/dotfiles/wiki" aria-label="Wiki" class="js-selected-navigation-item sunken-menu-item" data-pjax="true" data-selected-links="repo_wiki /garybernhardt/dotfiles/wiki">
            <span class="octicon octicon-book"></span> <span class="full-word">Wiki</span>
            <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>        </li>
    </ul>
    <div class="sunken-menu-separator"></div>
    <ul class="sunken-menu-group">

      <li class="tooltipped tooltipped-w" aria-label="Pulse">
        <a href="/garybernhardt/dotfiles/pulse" aria-label="Pulse" class="js-selected-navigation-item sunken-menu-item" data-pjax="true" data-selected-links="pulse /garybernhardt/dotfiles/pulse">
          <span class="octicon octicon-pulse"></span> <span class="full-word">Pulse</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

      <li class="tooltipped tooltipped-w" aria-label="Graphs">
        <a href="/garybernhardt/dotfiles/graphs" aria-label="Graphs" class="js-selected-navigation-item sunken-menu-item" data-pjax="true" data-selected-links="repo_graphs repo_contributors /garybernhardt/dotfiles/graphs">
          <span class="octicon octicon-graph"></span> <span class="full-word">Graphs</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

      <li class="tooltipped tooltipped-w" aria-label="Network">
        <a href="/garybernhardt/dotfiles/network" aria-label="Network" class="js-selected-navigation-item sunken-menu-item js-disable-pjax" data-selected-links="repo_network /garybernhardt/dotfiles/network">
          <span class="octicon octicon-git-branch"></span> <span class="full-word">Network</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>
    </ul>


  </div>
</div>

              <div class="only-with-full-nav">
                

  

<div class="clone-url open"
  data-protocol-type="http"
  data-url="/users/set_protocol?protocol_selector=http&amp;protocol_type=clone">
  <h3><strong>HTTPS</strong> clone URL</h3>
  <div class="clone-url-box">
    <input type="text" class="clone js-url-field"
           value="https://github.com/garybernhardt/dotfiles.git" readonly="readonly">

    <span aria-label="copy to clipboard" class="js-zeroclipboard url-box-clippy minibutton zeroclipboard-button" data-clipboard-text="https://github.com/garybernhardt/dotfiles.git" data-copied-hint="copied!"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>

  

<div class="clone-url "
  data-protocol-type="ssh"
  data-url="/users/set_protocol?protocol_selector=ssh&amp;protocol_type=clone">
  <h3><strong>SSH</strong> clone URL</h3>
  <div class="clone-url-box">
    <input type="text" class="clone js-url-field"
           value="git@github.com:garybernhardt/dotfiles.git" readonly="readonly">

    <span aria-label="copy to clipboard" class="js-zeroclipboard url-box-clippy minibutton zeroclipboard-button" data-clipboard-text="git@github.com:garybernhardt/dotfiles.git" data-copied-hint="copied!"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>

  

<div class="clone-url "
  data-protocol-type="subversion"
  data-url="/users/set_protocol?protocol_selector=subversion&amp;protocol_type=clone">
  <h3><strong>Subversion</strong> checkout URL</h3>
  <div class="clone-url-box">
    <input type="text" class="clone js-url-field"
           value="https://github.com/garybernhardt/dotfiles" readonly="readonly">

    <span aria-label="copy to clipboard" class="js-zeroclipboard url-box-clippy minibutton zeroclipboard-button" data-clipboard-text="https://github.com/garybernhardt/dotfiles" data-copied-hint="copied!"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>


<p class="clone-options">You can clone with
      <a href="#" class="js-clone-selector" data-protocol="http">HTTPS</a>,
      <a href="#" class="js-clone-selector" data-protocol="ssh">SSH</a>,
      or <a href="#" class="js-clone-selector" data-protocol="subversion">Subversion</a>.
  <span class="help tooltipped tooltipped-n" aria-label="Get help on which URL is right for you.">
    <a href="https://help.github.com/articles/which-remote-url-should-i-use">
    <span class="octicon octicon-question"></span>
    </a>
  </span>
</p>



                <a href="/garybernhardt/dotfiles/archive/master.zip"
                   class="minibutton sidebar-button"
                   aria-label="Download garybernhardt/dotfiles as a zip file"
                   title="Download garybernhardt/dotfiles as a zip file"
                   rel="nofollow">
                  <span class="octicon octicon-cloud-download"></span>
                  Download ZIP
                </a>
              </div>
        </div><!-- /.repository-sidebar -->

        <div id="js-repo-pjax-container" class="repository-content context-loader-container" data-pjax-container>
          


<!-- blob contrib key: blob_contributors:v21:fcf15e0f668af3a9ebc00c6dc20aca79 -->

<p title="This is a placeholder element" class="js-history-link-replace hidden"></p>

<a href="/garybernhardt/dotfiles/find/master" data-pjax data-hotkey="t" class="js-show-file-finder" style="display:none">Show File Finder</a>

<div class="file-navigation">
  

<div class="select-menu js-menu-container js-select-menu" >
  <span class="minibutton select-menu-button js-menu-target" data-hotkey="w"
    data-master-branch="master"
    data-ref="master"
    role="button" aria-label="Switch branches or tags" tabindex="0" aria-haspopup="true">
    <span class="octicon octicon-git-branch"></span>
    <i>branch:</i>
    <span class="js-select-button">master</span>
  </span>

  <div class="select-menu-modal-holder js-menu-content js-navigation-container" data-pjax aria-hidden="true">

    <div class="select-menu-modal">
      <div class="select-menu-header">
        <span class="select-menu-title">Switch branches/tags</span>
        <span class="octicon octicon-remove-close js-menu-close"></span>
      </div> <!-- /.select-menu-header -->

      <div class="select-menu-filters">
        <div class="select-menu-text-filter">
          <input type="text" aria-label="Filter branches/tags" id="context-commitish-filter-field" class="js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
        </div>
        <div class="select-menu-tabs">
          <ul>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="branches" class="js-select-menu-tab">Branches</a>
            </li>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="tags" class="js-select-menu-tab">Tags</a>
            </li>
          </ul>
        </div><!-- /.select-menu-tabs -->
      </div><!-- /.select-menu-filters -->

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="branches">

        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <div class="select-menu-item js-navigation-item selected">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/garybernhardt/dotfiles/blob/master/.vim/colors/grb256.vim"
                 data-name="master"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="master">master</a>
            </div> <!-- /.select-menu-item -->
        </div>

          <div class="select-menu-no-results">Nothing to show</div>
      </div> <!-- /.select-menu-list -->

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="tags">
        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


        </div>

        <div class="select-menu-no-results">Nothing to show</div>
      </div> <!-- /.select-menu-list -->

    </div> <!-- /.select-menu-modal -->
  </div> <!-- /.select-menu-modal-holder -->
</div> <!-- /.select-menu -->

  <div class="breadcrumb">
    <span class='repo-root js-repo-root'><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/garybernhardt/dotfiles" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">dotfiles</span></a></span></span><span class="separator"> / </span><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/garybernhardt/dotfiles/tree/master/.vim" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">.vim</span></a></span><span class="separator"> / </span><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/garybernhardt/dotfiles/tree/master/.vim/colors" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">colors</span></a></span><span class="separator"> / </span><strong class="final-path">grb256.vim</strong> <span aria-label="copy to clipboard" class="js-zeroclipboard minibutton zeroclipboard-button" data-clipboard-text=".vim/colors/grb256.vim" data-copied-hint="copied!"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>


  <div class="commit file-history-tease">
    <img alt="Gary Bernhardt" class="main-avatar js-avatar" data-user="45707" height="24" src="https://avatars0.githubusercontent.com/u/45707?s=140" width="24" />
    <span class="author"><a href="/garybernhardt" rel="author">garybernhardt</a></span>
    <time class="js-relative-date" data-title-format="YYYY-MM-DD HH:mm:ss" datetime="2011-04-28T20:53:34-07:00" title="2011-04-28 22:53:34">April 28, 2011</time>
    <div class="commit-title">
        <a href="/garybernhardt/dotfiles/commit/058a2fd5f3dccb0d09511edf81d3e07085881982" class="message" data-pjax="true" title="highlight selected item in completion menus">highlight selected item in completion menus</a>
    </div>

    <div class="participation">
      <p class="quickstat"><a href="#blob_contributors_box" rel="facebox"><strong>1</strong>  contributor</a></p>
      
    </div>
    <div id="blob_contributors_box" style="display:none">
      <h2 class="facebox-header">Users who have contributed to this file</h2>
      <ul class="facebox-user-list">
          <li class="facebox-user-list-item">
            <img alt="Gary Bernhardt" class=" js-avatar" data-user="45707" height="24" src="https://avatars0.githubusercontent.com/u/45707?s=140" width="24" />
            <a href="/garybernhardt">garybernhardt</a>
          </li>
      </ul>
    </div>
  </div>

<div class="file-box">
  <div class="file">
    <div class="meta clearfix">
      <div class="info file-name">
        <span class="icon"><b class="octicon octicon-file-text"></b></span>
        <span class="mode" title="File Mode">file</span>
        <span class="meta-divider"></span>
          <span>33 lines (23 sloc)</span>
          <span class="meta-divider"></span>
        <span>1.664 kb</span>
      </div>
      <div class="actions">
        <div class="button-group">
                <a class="minibutton tooltipped tooltipped-n js-update-url-with-hash"
                   aria-label="Clicking this button will automatically fork this project so you can edit the file"
                   href="/garybernhardt/dotfiles/edit/master/.vim/colors/grb256.vim"
                   data-method="post" rel="nofollow">Edit</a>
          <a href="/garybernhardt/dotfiles/raw/master/.vim/colors/grb256.vim" class="button minibutton " id="raw-url">Raw</a>
            <a href="/garybernhardt/dotfiles/blame/master/.vim/colors/grb256.vim" class="button minibutton js-update-url-with-hash">Blame</a>
          <a href="/garybernhardt/dotfiles/commits/master/.vim/colors/grb256.vim" class="button minibutton " rel="nofollow">History</a>
        </div><!-- /.button-group -->

            <a class="minibutton danger empty-icon tooltipped tooltipped-s"
               href="/garybernhardt/dotfiles/delete/master/.vim/colors/grb256.vim"
               aria-label="Fork this project and delete file"
               data-method="post" data-test-id="delete-blob-file" rel="nofollow">

          Delete
        </a>
      </div><!-- /.actions -->
    </div>
        <div class="blob-wrapper data type-viml js-blob-data">
        <table class="file-code file-diff tab-size-8">
          <tr class="file-code-line">
            <td class="blob-line-nums">
              <span id="L1" rel="#L1">1</span>
<span id="L2" rel="#L2">2</span>
<span id="L3" rel="#L3">3</span>
<span id="L4" rel="#L4">4</span>
<span id="L5" rel="#L5">5</span>
<span id="L6" rel="#L6">6</span>
<span id="L7" rel="#L7">7</span>
<span id="L8" rel="#L8">8</span>
<span id="L9" rel="#L9">9</span>
<span id="L10" rel="#L10">10</span>
<span id="L11" rel="#L11">11</span>
<span id="L12" rel="#L12">12</span>
<span id="L13" rel="#L13">13</span>
<span id="L14" rel="#L14">14</span>
<span id="L15" rel="#L15">15</span>
<span id="L16" rel="#L16">16</span>
<span id="L17" rel="#L17">17</span>
<span id="L18" rel="#L18">18</span>
<span id="L19" rel="#L19">19</span>
<span id="L20" rel="#L20">20</span>
<span id="L21" rel="#L21">21</span>
<span id="L22" rel="#L22">22</span>
<span id="L23" rel="#L23">23</span>
<span id="L24" rel="#L24">24</span>
<span id="L25" rel="#L25">25</span>
<span id="L26" rel="#L26">26</span>
<span id="L27" rel="#L27">27</span>
<span id="L28" rel="#L28">28</span>
<span id="L29" rel="#L29">29</span>
<span id="L30" rel="#L30">30</span>
<span id="L31" rel="#L31">31</span>
<span id="L32" rel="#L32">32</span>

            </td>
            <td class="blob-line-code"><div class="code-body highlight"><pre><div class='line' id='LC1'><span class="c">&quot; Based on</span></div><div class='line' id='LC2'>runtime <span class="k">colors</span>/ir_black.<span class="k">vim</span></div><div class='line' id='LC3'><br/></div><div class='line' id='LC4'><span class="k">let</span> <span class="k">g</span>:colors_name <span class="p">=</span> <span class="s2">&quot;grb256&quot;</span></div><div class='line' id='LC5'><br/></div><div class='line' id='LC6'><span class="k">hi</span> pythonSpaceError ctermbg<span class="p">=</span><span class="k">red</span> guibg<span class="p">=</span><span class="k">red</span></div><div class='line' id='LC7'><br/></div><div class='line' id='LC8'><span class="k">hi</span> Comment ctermfg<span class="p">=</span>darkgray</div><div class='line' id='LC9'><br/></div><div class='line' id='LC10'><span class="k">hi</span> StatusLine ctermbg<span class="p">=</span>darkgrey ctermfg<span class="p">=</span>white</div><div class='line' id='LC11'><span class="k">hi</span> StatusLineNC ctermbg<span class="p">=</span>black ctermfg<span class="p">=</span>lightgrey</div><div class='line' id='LC12'><span class="k">hi</span> VertSplit ctermbg<span class="p">=</span>black ctermfg<span class="p">=</span>lightgrey</div><div class='line' id='LC13'><span class="k">hi</span> LineNr ctermfg<span class="p">=</span>darkgray</div><div class='line' id='LC14'><span class="k">hi</span> CursorLine     guifg<span class="p">=</span><span class="nb">NONE</span>        guibg<span class="p">=</span><span class="mh">#121212</span>     <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span>      ctermfg<span class="p">=</span><span class="nb">NONE</span>        ctermbg<span class="p">=</span><span class="m">234</span></div><div class='line' id='LC15'><span class="k">hi</span> Function         guifg<span class="p">=</span>#FFD2A7     guibg<span class="p">=</span><span class="nb">NONE</span>        <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span>      ctermfg<span class="p">=</span>yellow       ctermbg<span class="p">=</span><span class="nb">NONE</span>        cterm<span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC16'><span class="k">hi</span> Visual           guifg<span class="p">=</span><span class="nb">NONE</span>        guibg<span class="p">=</span>#<span class="m">262</span>D<span class="m">51</span>     <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span>      ctermfg<span class="p">=</span><span class="nb">NONE</span>        ctermbg<span class="p">=</span><span class="m">236</span>    cterm<span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC17'><br/></div><div class='line' id='LC18'><span class="k">hi</span> <span class="k">Error</span>            guifg<span class="p">=</span><span class="nb">NONE</span>        guibg<span class="p">=</span><span class="nb">NONE</span>        <span class="k">gui</span><span class="p">=</span>undercurl ctermfg<span class="p">=</span><span class="m">16</span>       ctermbg<span class="p">=</span><span class="k">red</span>         cterm<span class="p">=</span><span class="nb">NONE</span>     guisp<span class="p">=</span>#FF6C60 <span class="c">&quot; undercurl color</span></div><div class='line' id='LC19'><span class="k">hi</span> ErrorMsg         guifg<span class="p">=</span>white       guibg<span class="p">=</span>#FF6C60     <span class="k">gui</span><span class="p">=</span>BOLD      ctermfg<span class="p">=</span><span class="m">16</span>       ctermbg<span class="p">=</span><span class="k">red</span>         cterm<span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC20'><span class="k">hi</span> WarningMsg       guifg<span class="p">=</span>white       guibg<span class="p">=</span>#FF6C60     <span class="k">gui</span><span class="p">=</span>BOLD      ctermfg<span class="p">=</span><span class="m">16</span>       ctermbg<span class="p">=</span><span class="k">red</span>         cterm<span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC21'><span class="k">hi</span> SpellBad       guifg<span class="p">=</span>white       guibg<span class="p">=</span>#FF6C60     <span class="k">gui</span><span class="p">=</span>BOLD      ctermfg<span class="p">=</span><span class="m">16</span>       ctermbg<span class="p">=</span><span class="m">160</span>         cterm<span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC22'><br/></div><div class='line' id='LC23'><span class="c">&quot; ir_black doesn&#39;t highlight operators for some reason</span></div><div class='line' id='LC24'><span class="k">hi</span> Operator        guifg<span class="p">=</span>#<span class="m">6699</span>CC     guibg<span class="p">=</span><span class="nb">NONE</span>        <span class="k">gui</span><span class="p">=</span><span class="nb">NONE</span>      ctermfg<span class="p">=</span>lightblue   ctermbg<span class="p">=</span><span class="nb">NONE</span>        cterm<span class="p">=</span><span class="nb">NONE</span></div><div class='line' id='LC25'><br/></div><div class='line' id='LC26'><span class="nb">highlight</span> DiffAdd <span class="nb">term</span><span class="p">=</span>reverse cterm<span class="p">=</span><span class="nb">bold</span> ctermbg<span class="p">=</span>lightgreen ctermfg<span class="p">=</span><span class="m">16</span></div><div class='line' id='LC27'><span class="nb">highlight</span> DiffChange <span class="nb">term</span><span class="p">=</span>reverse cterm<span class="p">=</span><span class="nb">bold</span> ctermbg<span class="p">=</span>lightblue ctermfg<span class="p">=</span><span class="m">16</span></div><div class='line' id='LC28'><span class="nb">highlight</span> DiffText <span class="nb">term</span><span class="p">=</span>reverse cterm<span class="p">=</span><span class="nb">bold</span> ctermbg<span class="p">=</span>lightgray ctermfg<span class="p">=</span><span class="m">16</span></div><div class='line' id='LC29'><span class="nb">highlight</span> DiffDelete <span class="nb">term</span><span class="p">=</span>reverse cterm<span class="p">=</span><span class="nb">bold</span> ctermbg<span class="p">=</span>lightred ctermfg<span class="p">=</span><span class="m">16</span></div><div class='line' id='LC30'><br/></div><div class='line' id='LC31'><span class="nb">highlight</span> PmenuSel ctermfg<span class="p">=</span><span class="m">16</span> ctermbg<span class="p">=</span><span class="m">156</span></div><div class='line' id='LC32'><br/></div></pre></div></td>
          </tr>
        </table>
  </div>

  </div>
</div>

<a href="#jump-to-line" rel="facebox[.linejump]" data-hotkey="l" class="js-jump-to-line" style="display:none">Jump to Line</a>
<div id="jump-to-line" style="display:none">
  <form accept-charset="UTF-8" class="js-jump-to-line-form">
    <input class="linejump-input js-jump-to-line-field" type="text" placeholder="Jump to line&hellip;" autofocus>
    <button type="submit" class="button">Go</button>
  </form>
</div>

        </div>

      </div><!-- /.repo-container -->
      <div class="modal-backdrop"></div>
    </div><!-- /.container -->
  </div><!-- /.site -->


    </div><!-- /.wrapper -->

      <div class="container">
  <div class="site-footer">
    <ul class="site-footer-links right">
      <li><a href="https://status.github.com/">Status</a></li>
      <li><a href="http://developer.github.com">API</a></li>
      <li><a href="http://training.github.com">Training</a></li>
      <li><a href="http://shop.github.com">Shop</a></li>
      <li><a href="/blog">Blog</a></li>
      <li><a href="/about">About</a></li>

    </ul>

    <a href="/">
      <span class="mega-octicon octicon-mark-github" title="GitHub"></span>
    </a>

    <ul class="site-footer-links">
      <li>&copy; 2014 <span title="0.04648s from github-fe135-cp1-prd.iad.github.net">GitHub</span>, Inc.</li>
        <li><a href="/site/terms">Terms</a></li>
        <li><a href="/site/privacy">Privacy</a></li>
        <li><a href="/security">Security</a></li>
        <li><a href="/contact">Contact</a></li>
    </ul>
  </div><!-- /.site-footer -->
</div><!-- /.container -->


    <div class="fullscreen-overlay js-fullscreen-overlay" id="fullscreen_overlay">
  <div class="fullscreen-container js-fullscreen-container">
    <div class="textarea-wrap">
      <textarea name="fullscreen-contents" id="fullscreen-contents" class="fullscreen-contents js-fullscreen-contents" placeholder="" data-suggester="fullscreen_suggester"></textarea>
    </div>
  </div>
  <div class="fullscreen-sidebar">
    <a href="#" class="exit-fullscreen js-exit-fullscreen tooltipped tooltipped-w" aria-label="Exit Zen Mode">
      <span class="mega-octicon octicon-screen-normal"></span>
    </a>
    <a href="#" class="theme-switcher js-theme-switcher tooltipped tooltipped-w"
      aria-label="Switch themes">
      <span class="octicon octicon-color-mode"></span>
    </a>
  </div>
</div>



    <div id="ajax-error-message" class="flash flash-error">
      <span class="octicon octicon-alert"></span>
      <a href="#" class="octicon octicon-remove-close close js-ajax-error-dismiss"></a>
      Something went wrong with that request. Please try again.
    </div>

  </body>
</html>

