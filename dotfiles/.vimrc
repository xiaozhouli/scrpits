""""""""""""""""""""""""""""""""""""""""""
" This vimrc is used for Mac 10.11
" Author: Xiaozhou Li
" Creat Time:   May 28, 2013
" Last Changed: Mar 11, 2016
"
" Sections:
"   -> Vundle plugin manager
"	-> General
"	-> VIM user interface
"	-> Visual
"	-> Fortran section
"	-> C/C++ section
"	-> Tex section
""""""""""""""""""""""""""""""""""""""""""
"auto reload when saving
autocmd! bufwritepost .vimrc source %

" activate Vundle
set nocompatible              " required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
" call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Add all plugins here 
" Python folding, https://github.com/tmhedberg/SimpylFold
Plugin 'tmhedberg/SimpylFold'

Plugin 'vim-scripts/indentpython.vim'

Plugin 'Valloric/YouCompleteMe'

Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'

" color schemes
Plugin 'jnurmine/Zenburn'
Plugin 'altercation/vim-colors-solarized'

" Syntax Checking
Plugin 'scrooloose/syntastic'
"  python
Plugin 'nvie/vim-flake8'

" vim-latex suit 
Plugin 'vim-latex/vim-latex'
Plugin 'gi1242/vim-tex-syntax'

" git command
Plugin 'tpope/vim-fugitive'

" All of Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required


"""""""""""""""""
" Get out of VI's compatible mode
set nocompatible

" UTF8 Support
set encoding=utf-8

" Sets how many lines of history VIM have to remember
set history=400

"
let mapleader=","

" Disable localized menus
set langmenu=none

" Enable filetype plugin
filetype on
filetype plugin on
filetype indent on

" Set to auto read when a file is changed from the outside
set autoread

" Have the mouse enabled all the time
set mouse=a

" Set working directory to the current file
set autochdir
"autocmd BufEnter * silent! lcd %:p:h

""""""""""""""""""""""""""""""""""
" 	VIM userinterface
""""""""""""""""""""""""""""""""""

" Always showw current position
set ruler

" The commandbar is 2 high
set cmdheight=2

" Change 'backspace' to more sensible value from the default
set backspace=indent,eol,start

" Show line number
set number

" Ignore case when search
set ignorecase

" Make search act like search in modern browers
set incsearch

" Highlight search things
set hlsearch

" No sound on errors.
set noerrorbells
set novisualbell

" Show matching bracets
set showmatch

" Show the command we are typing
set showcmd

" line wrapping
set textwidth=80

set wrap
set linebreak

" No swap files
set nobackup
set nowritebackup
set noswapfile

" Enable folding
set foldmethod=indent
set foldlevel=99
" " Enable folding with the spacebar 
nnoremap <space> za 


" Set tabs
set shiftwidth=4
set expandtab
set softtabstop=4
set tabstop=4
set autoindent
set smartindent

"""""""""""""""""""""""""""""
" 	Visual 
" """""""""""""""""""""""""""

" Enable syntax highlight
syntax enable
syntax on

set background=dark

if has('gui_running')
    set background=dark
    colorscheme solarized
else
    colorscheme zenburn
endif

" Font
set guifont=DejaVu\ Sans\ Mono:h14

"set lines=30 columns=70
" Window size
if has("gui_running")
    " GUI is running or is about start.
    "
    set lines=48 columns=125
"else
    " This is console Vim.
"    if exists("+lines")
"        set lines=50
"    endif
"    if exists("+columns")
"        set columns=100
"    endif
endif

if has("gui_vimr")
    " VimR specific stuff
    
    let loaded_nerd_tree=1
    set columns=100
endif

if has("gui_macvim")
    " MacVim specific stuff
    "
endif

"""""""""""""""""""""""""""""
" 	Fortran
"""""""""""""""""""""""""""""
" Free Format
let fortran_free_source=1

" Fixed source form for f77
let s:extfname=expand("%:e")
if s:extfname==? "f77"
    let fortran_fixed_source=1
    unlet! fortran_free_source
endif

" Do loops will be indented
let fortran_do_enddo=1

" Set contain tabs
let fortran_have_tabs=1

" Set tabs and indent
autocmd FileType fortran set tabstop=4
autocmd FileType fortran set cindent shiftwidth=4
autocmd FileType fortran set autoindent shiftwidth=4
autocmd FileType fortran set expandtab

" Set colorscheme
"autocmd FileType fortran colorscheme graywh

" More precise fortran syntax
:let fortran_more_precise=1


"""""""""""""""""""""""""""""""
" 	C/C++
"""""""""""""""""""""""""""""""

" Set tabs and indent
autocmd FileType c,cpp set tabstop=4
autocmd FileType c,cpp set cindent shiftwidth=4
autocmd FileType c,cpp set autoindent shiftwidth=4
autocmd FileType c,cpp set expandtab

" Set colorscheme
autocmd FileType c,cpp colorscheme lucius


"""""""""""""""""""""""""""""""
" 	Latex
"""""""""""""""""""""""""""""""
" Latex-Suit
"  grep will sometimes skip displaying the file name if you search in a singe file. This will confuse Latex-Suite. Set your grep program to always generate a file-name.
set grepprg=grep\ -nH\ $*
let g:tex_flavor='latex'
let g:Tex_TreatMacViewerAsUNIX = 1
let g:Tex_ExecuteUNIXViewerInForeground = 1
let g:Tex_ViewRule_ps = 'open -a Preview'
let g:Tex_ViewRule_pdf = 'open -a Preview'
let g:Tex_ViewRule_dvi = 'open -a Preview'

"""""""""""""""""""""""""""""""
"       Python
"""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""
"       Plugin Setting
"""""""""""""""""""""""""""""""
"""
let g:SimpylFold_docstring_preview=1

"""
"nerdtree setting
let g:NERDTreeMouseMode=2
let g:NERDTreeWinSize=25

autocmd vimenter * NERDTree "start nerdtree automatically
"let g:nerdtree_tabs_open_on_gui_startup=0
" close window if nerdtree is the only thing open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
"""

"syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:ycm_path_to_python_interpreter = '/usr/bin/python'
