#! /bin/bash

# Created by Xiaozhou Li, 2017-06-01

# gcc, openmpi from Macports

# gcc version
GCC_VERSION="6.3.0"

# openmpi version 
OPENMPI_VERSION="1.10.3"

# name of the soft
SOFT_NAME="petsc"

# version of the soft
SOFT_VERSION="3.7.6"

# address of the repository
SOFT_REPO="http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/${SOFT_NAME}-${SOFT_VERSION}.tar.gz"

# name of the downloaded file
SOFT_FILE="${SOFT_NAME}-${SOFT_VERSION}.tar.gz"

# archive type (gzip: xzf / bzip2: xjf)
ARC_TYPE="xzf"

# configuration
SOFT_CONF="${SOFT_NAME}-${SOFT_VERSION}/configure"

# directories
WORK_DIR="/Users/xiaozhouli/soft/${SOFT_NAME}/${SOFT_VERSION}/src"
INSTALL_DIR="/Users/xiaozhouli/soft/${SOFT_NAME}/${SOFT_VERSION}/gcc-${GCC_VERSION}/"

# generate the work directory
mkdir -pv $WORK_DIR
cd $WORK_DIR

# download source files (if not yet download)
if [ ! -f ${SOFT_FILE} ]; then
    wget ${SOFT_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${SOFT_FILE}

###############################################
###				 EXPORT VARIABLES:
###############################################

CC=mpicc
CXX=mpicxx
FC=mpif90
F77=mpif77
F90=mpif90                                    
CFLAGS='-fPIC -fopenmp'
CXXFLAGS='-fPIC -fopenmp'
FFLAGS='-fPIC -fopenmp'
FCFLAGS='-fPIC -fopenmp'
F90FLAGS='-fPIC -fopenmp'
F77FLAGS='-fPIC -fopenmp'        

###############################################

# create the build directory
mkdir -pv ${WORK_DIR}/build
cd build

cd ${WORK_DIR}/${SOFT_NAME}-${SOFT_VERSION}

# build
python2 './configure' '--prefix==${INSTALL_DIR}' '--download-hypre=1' \
'--with-ssl=0' '--with-debugging=no' '--with-pic=1' '--with-shared-libraries=1'\
'--with-cc=mpicc' '--with-cxx=mpicxx' '--with-fc=mpif90' \
'--download-fblaslapack=1' '--download-metis=1' '--download-parmetis=1' \
'--download-superlu_dist=1' '--download-mumps=1' '--download-scalapack=1' 

# make & install
make && \
make install
